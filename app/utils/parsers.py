from base64 import b64encode


def parse_to_base64(binary_value):
    return b64encode(binary_value).decode('ascii')
