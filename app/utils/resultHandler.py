from app.utils import messages

namespaces = {
    'fe': 'http://www.facturae.es/Facturae/2014/v3.2.1/Facturae',
    'css': 'http://www.safelayer.com/TWS',
    'dss': 'http://www.docs.oasis-open.org/dss/2004/06/oasis-dss-1.0-core-schema-wd-27.xsd',
    'xades': 'http://uri.etsi.org/01903/v1.2.2#',
    'SOAP-ENV': 'http://schemas.xmlsoap.org/soap/envelope/'
}


def prettify_verification(result, invoice):
    body = result.find('SOAP-ENV:Body', namespaces)
    verification = body.find('dss:VerifyResponse', namespaces)
    inner_result = verification.find('dss:Result', namespaces)
    major_result = inner_result.find('dss:ResultMajor', namespaces)
    minor_result = inner_result.find('dss:ResultMinor', namespaces)
    result_message = inner_result.find('dss:ResultMessage', namespaces)

    print(f'\n{messages.VERIFICATION_RESULT}\n')
    major_result_value = major_result.text.split(':')[-1]
    print(f'{messages.MAJOR_RESULT}{major_result_value}')
    minor_result_value = minor_result.text.split(':')[-1]
    print(f'{messages.MINOR_RESULT}{minor_result_value}')

    if result_message is not None:
        print(f'\n{messages.COMPLEMENTARY_INFO}\n')
        print(f'{messages.RESULT_MESSAGE}{result_message.text}')

    if minor_result_value == 'ValidSignature_OnAllDocuments':
        optional_info = verification.find('dss:OptionalOutputs', namespaces)
        signing_time = optional_info.find('dss:SigningTime', namespaces)
        signer_identity = optional_info.find('dss:SignerIdentity', namespaces)

        print(f'\n{messages.COMPLEMENTARY_INFO}\n')
        print(f'{messages.FILE_INTEGRITY}No')
        print(f'{messages.SIGNING_TIME}{signing_time.text}')
        print(f'{messages.SIGNER_IDENTITY}{signer_identity.text}')

        prettify_invoice(invoice)


def prettify_invoice(invoice):
    root = invoice.getroot()
    header = root.find('FileHeader', namespaces)
    batch = header.find('Batch', namespaces)
    currency = batch.find('InvoiceCurrencyCode', namespaces)
    executable_amount = batch.find('TotalExecutableAmount', namespaces)
    total_amount = executable_amount.find('TotalAmount', namespaces)
    parties = root.find('Parties', namespaces)
    seller = parties.find('SellerParty', namespaces)
    legal_info = seller.find('LegalEntity', namespaces)
    name = legal_info.find('CorporateName', namespaces)

    print(f'{messages.SENDER}{name.text}')
    print(f'{messages.TOTAL_AMOUNT}{total_amount.text}')
    print(f'{messages.CURRENCY}{currency.text}')
