WELCOME = '''************************************************************************************
* Bienvendi@ al verificador de firmas digitales para facturas en formato Factura-e *
************************************************************************************
'''
FILE_REQUEST = 'Por favor especifique la ubicación del archivo que desea verificar: '
INVALID_FILE = 'El archivo especificado no es válido.'
VERIFYING = 'Verificando...'
VERIFICATION_RESULT = '*************** RESULTADO ***************'
MAJOR_RESULT = 'Ejecución    : '
MINOR_RESULT = 'Verificación : '
RESULT_MESSAGE = 'Observaciones: '
COMPLEMENTARY_INFO = '*************** INFORMACIÓN COMPLEMENTARIA ***************'
FILE_INTEGRITY = 'El archivo ha sido modificado   : '
SIGNING_TIME = 'Fecha cuando se realizó la firma: '
SIGNER_IDENTITY = 'Emisor de la factura            : '
SENDER = 'Remitente de la factura         : '
CURRENCY = 'Tipo de moneda                  : '
TOTAL_AMOUNT = 'Valor total                     : '
BYE = '¡Adiós!'
