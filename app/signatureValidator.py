from os import path
from xml.etree import ElementTree
from warnings import catch_warnings, simplefilter

from requests import post
from urllib3.exceptions import InsecureRequestWarning

from app.templates.verification_request import request_body
from app.utils import messages
from app.utils.parsers import parse_to_base64
from app.utils.resultHandler import prettify_verification


class SignatureValidator:
    verification = None
    invoice = None

    def run(self):
        try:
            print(messages.WELCOME)
            filename = input(messages.FILE_REQUEST).strip()

            if not path.isfile(filename) or not path.exists(filename):
                print(messages.INVALID_FILE)
                return

            self.invoice = ElementTree.parse(filename)

            with open(filename, mode='rb') as xml_invoice:
                parsed_invoice = parse_to_base64(xml_invoice.read())
                self.check_signature(parsed_invoice)

            self.show_results()
            print(f'\n{messages.BYE}')

        except KeyboardInterrupt:
            print(f'\n{messages.BYE}')

    def check_signature(self, parsed_invoice):
        verification_service = 'https://uoc.safelayer.com:8080/trustedx-gw/SoapGateway'
        headers = {'Content-Type': 'application/xml', 'SOAPAction': 'verify'}
        body = request_body.replace('{{encoded_invoice}}', parsed_invoice)

        with catch_warnings():
            simplefilter('ignore', category=InsecureRequestWarning)
            print(f'\n{messages.VERIFYING}')
            response = post(verification_service, data=body, headers=headers, verify=False)

        self.verification = ElementTree.fromstring(response.content)

    def show_results(self):
        prettify_verification(self.verification, self.invoice)
