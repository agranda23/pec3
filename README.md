# PEC3: Verificador de firmas en facturas electrónicas

## Configuración del ambiente de desarrollo

Para configurar el ambiente de desarrollo se deben instalar las dependencias del proyecto utilizando el siguiente comando.

```bash
$ pip install -r requirements.txt
```

## Convenciones

El código se debe realizar en inglés y únicamente los mensajes que se muestran al usuario se pueden escribir en otro idioma.


## Generar archivos ejecutables

Para generar archivos ejecutables es necesario utilizar la librería `pyinstaller`.

Una vez instalada la librería para generar un ejecutable del proyecto se debe ejecutar el siguiente comando.

```bash
$ pyinstaller main.py --name verificador --onefile
```

Luego de ejecutar el comando anterior un archivo ejecutable de nombre verificador estará ubicado en directorio dist del proyecto.

**Nota:** El ejecutable solo es compatible con la plataforma en la que fue generado. Es decir, si el ejecutable se genero en un sistema Linux solo se podrá ejecutar en un sistema Linux.


## Ejecutar el programa

Para ejecutar el programa desde el código fuente se debe utilizar el siguiente comando.

```bash
$ python main.py
```
